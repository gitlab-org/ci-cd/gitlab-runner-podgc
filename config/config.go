package config

import (
	"fmt"
	"time"

	"github.com/kelseyhightower/envconfig"
)

var (
	minimunInterval             = time.Second
	minimumRequestTimeout       = 5 * time.Second
	minimumCacheExpiration      = time.Minute
	minimumCacheCleanupInterval = time.Minute
	minimumRequestLimit         = 100
)

//nolint:lll
type KubernetesConfig struct {
	RequestLimit   int      `split_words:"true" default:"500" description:"Limit the number of pods to retrieve per API request when getting existing pod"`
	RequestTimeout string   `split_words:"true" default:"30s" description:"The maximum amount of time a Kubernetes API request can take"`
	Namespaces     []string `split_words:"true" default:"default" description:"List of namespaces to search for Kubernetes pods"`
	Annotation     string   `split_words:"true" default:"pod-cleanup.gitlab.com/ttl" description:"List of annotations to consider for the ttl setting"`
}

//nolint:lll
type Config struct {
	LogLevel             string `split_words:"true" default:"info" description:"Log level for the application"`
	LogFormat            string `split_words:"true" default:"json" description:"Accepted values: text and json"`
	Interval             string `split_words:"true" default:"60s" description:"Deletion interval"`
	CacheCleanupInterval string `split_words:"true" default:"30m" description:"Cache cleanup interval"`
	CacheExpiration      string `split_words:"true" default:"1.5h" description:"Default expiration time for cached data"`
	MaxErrAllowed        int    `split_words:"true" default:"5" description:"The maximum number of errors allowed when deleting pods on a given namespace. Once this limit is reached, the pod cleanup exits for the given namespace"`
	Limit                int    `split_words:"true" default:"15" description:"Maximum number of Pods to be deleted per each POD_CLEANUP_INTERVAL tick"`
	Kubernetes           KubernetesConfig
}

func (c Config) GetInterval() (time.Duration, error) {
	duration, err := time.ParseDuration(c.Interval)
	if err != nil {
		return 0, fmt.Errorf("parsing Interval %w", err)
	}

	if duration < minimunInterval {
		return minimunInterval, nil
	}

	return duration, nil
}

func (c Config) GetRequestTimeout() (time.Duration, error) {
	duration, err := time.ParseDuration(c.Kubernetes.RequestTimeout)
	if err != nil {
		return 0, fmt.Errorf("parsing Kubernetes RequestTimeout %w", err)
	}

	if duration < minimumRequestTimeout {
		return minimumRequestTimeout, nil
	}

	return duration, nil
}

func (c Config) GetCacheCleanupInterval() (time.Duration, error) {
	duration, err := time.ParseDuration(c.CacheCleanupInterval)
	if err != nil {
		return 0, fmt.Errorf("parsing CacheCleanupInterval %w", err)
	}

	if duration < minimumCacheCleanupInterval {
		return minimumCacheCleanupInterval, nil
	}

	return duration, nil
}

func (c Config) GetCacheExpiration() (time.Duration, error) {
	duration, err := time.ParseDuration(c.CacheExpiration)
	if err != nil {
		return 0, fmt.Errorf("parsing CacheExpiration %w", err)
	}

	if duration < minimumCacheExpiration {
		return minimumCacheExpiration, nil
	}

	return duration, nil
}

func (c Config) GetRequestLimit() int {
	if c.Kubernetes.RequestLimit < minimumRequestLimit {
		return minimumRequestLimit
	}

	return c.Kubernetes.RequestLimit
}

func New() Config {
	var c Config
	envconfig.MustProcess("POD_CLEANUP", &c)
	return c
}
