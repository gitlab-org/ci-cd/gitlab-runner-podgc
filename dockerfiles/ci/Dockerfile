ARG DOCKER_VERSION
FROM docker:${DOCKER_VERSION}-git

RUN set -x \
    && apk upgrade \
    && apk add make py3-pip bash curl openconnect psmisc gcc musl-dev

ARG GO_VERSION
ARG GO_SHASUM
ARG KUBECTL_VERSION

ENV CGO_ENABLED=1

RUN set -x \
    && cd /usr/local \
    && curl -OL https://golang.org/dl/go${GO_VERSION}.linux-amd64.tar.gz \
    && echo "${GO_SHASUM} go${GO_VERSION}.linux-amd64.tar.gz" | sha256sum -c - \
    && tar -xf go${GO_VERSION}.linux-amd64.tar.gz \
    && rm go${GO_VERSION}.linux-amd64.tar.gz \
    && ln -s /usr/local/go/bin/* /usr/local/bin \
    && go version

# Install kubectl
RUN curl -OL https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl \
    && chmod +x kubectl \
    && mv kubectl /usr/local/bin/ \
    && kubectl version --client
