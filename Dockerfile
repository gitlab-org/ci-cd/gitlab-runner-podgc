FROM golang:1.21.1 as builder

WORKDIR /pod-cleanup
ADD . .

RUN VERSION=$(echo $(scripts/version 2>/dev/null || echo "dev") | sed -e 's/^v//g') && \
    REVISION=$(git rev-parse --short HEAD || echo "unknown") && \
    BRANCH=$(git show-ref | grep "$(REVISION)" | grep -v HEAD | awk '{print $$2}' | sed 's|refs/remotes/origin/||' | sed 's|refs/heads/||' | sort | head -n 1) && \
    BUILT=$(date -u +%Y-%m-%dT%H:%M:%S%z) && \
    PKG=$(go list .) && \
    GO_LDFLAGS=$(echo "-X '${PKG}.VERSION=${VERSION}' -X '${PKG}.REVISION=${REVISION}' -X '${PKG}.BRANCH=${BRANCH}' -X '${PKG}.BUILT=${BUILT}' -s -w") && \
    echo ${GO_LDFLAGS} && \
    CGO_ENABLED=0 GOOS=linux \
    go build -ldflags="${GO_LDFLAGS} -extldflags '-static'" -o /app \
    && chmod +x /app

FROM alpine:3.18

COPY --from=builder /etc/ssl/certs/ca-certificates.crt \
    /etc/ssl/certs/ca-certificates.crt

COPY --from=builder /app /app

USER 1000:1000

ENTRYPOINT ["/app"]
